package main

import "fmt"

func main() {
	//fmt.Println("hello world")
	//
	var age float64 = 12.213
	var num float64 = 2.349444
	var word string = "разность равна :"
	var summ float64
	fmt.Println(age, num)
	fmt.Print(age, "\n делим на:", num)
	summ = age / num
	fmt.Print("\n"+word, summ)
	fmt.Print("\n вывод данных при другом формате")
	fmt.Printf("%.2f \n", num)
	fmt.Println("\n ******** слитность" + "\n" + "\n")
	//
	if summ < 3 {
		fmt.Println("Нипалучилось не фортануло")
	} else if summ == 5.198251160700148 {
		fmt.Println("палучилось фортануло")
	} else if summ > 6 {
		fmt.Println("пацаны вообще ребята, умеете МоГЁТе")
	}
	//
	fmt.Println("\n ******** слитность" + "\n" + "\n")
	fmt.Println("циколы *******")
	var arr [101]float64
	var i = 0
	var j float64 = 0
	for i <= 100 {
		j++
		arr[i] = j * age
		i++
		switch i {
		case 10:
			fmt.Println("возможно")
		case 20:
			fmt.Println("не возможно")
		case 30:
			fmt.Println("не думаю")
		case 40:
			fmt.Println("наверное")
		case 50:
			fmt.Println("скорее всего")
		case 60:
			fmt.Println("врядли")
		case 70:
			fmt.Println("более")
		case 80:
			fmt.Println("менее")
		case 90:
			fmt.Println("более менее")
		case 100:
			fmt.Println("точно")
		default:
			fmt.Println("я *** знает если честно \n")
		}
	}
	//
	fmt.Println("\n---------------------------------------" + "\n---------------------------------------" + "\n---------------------------------------" + "\n---------------------------------------")
	var a = 1
	for a <= 99 {
		a++
		fmt.Println(arr[a])
	}
	//
	fmt.Println("\n---------------------------------------" + "\n---------------------------------------" + "\n---------------------------------------" + "\n---------------------------------------")
	fmt.Println("\n---------------------------------------" + "\n---------------------------------------" + "\n---------------------------------------" + "\n---------------------------------------")

	webSites := make(map[string]string)
	webSites["google.com"] = "ping 8.8.8.8"
	webSites["yadnex.com"] = "2"
	fmt.Println(webSites["google.com"])

	//пока всё
}
